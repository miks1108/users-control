<?php

namespace miks1108\usersControl\console\services\createUser;

/**
 * Interface Method
 */
interface Method
{
    /**
     * @return string
     */
    public function getUsername(): string;

    /**
     * @return string
     */
    public function getPassword(): string;

    /**
     * @return string
     */
    public function getEmail(): string;
}
