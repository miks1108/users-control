<?php

namespace miks1108\usersControl\console\services\createUser;

use common\models\User;
use miks1108\usersControl\UsersControlModule;
use Yii;
use yii\helpers\{
    ArrayHelper,
    Console
};

/**
 * Class ManualMethod
 */
class ManualMethod implements Method
{
    use ManualMethodValidatorsTrait;

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->getValueByAttribute(
            'username',
            [
                'validator' => function ($input, &$error) {
                    $this->trim($input);
                    if (!$this->validateRequired('username', $input, $error)) {
                        return false;
                    }
                    $usernameMinLength = UsersControlModule::getDefaultStringMinLength();
                    if (!$this->validateMinLength('username', $input, $error, $usernameMinLength)) {
                        return false;
                    }
                    if (!$this->validateMaxLength('username', $input, $error)) {
                        return false;
                    }
                    if (!$this->validateExists('username', $input, User::class, $error)) {
                        return false;
                    }

                    return true;
                }
            ]
        );
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->getValueByAttribute(
            'password',
            [
                'validator' => function ($input, &$error) {
                    $this->trim($input);
                    if (!$this->validateRequired('password', $input, $error)) {
                        return false;
                    }
                    $passwordMinLength = UsersControlModule::getPasswordMinLength();
                    if (!$this->validateMinLength('password', $input, $error, $passwordMinLength)) {
                        return false;
                    }

                    return true;
                }
            ]
        );
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->getValueByAttribute(
            'email',
            [
                'validator' => function ($input, &$error) {
                    $this->trim($input);
                    if (!$this->validateRequired('email', $input, $error)) {
                        return false;
                    }
                    if (!$this->validateMaxLength('email', $input, $error)) {
                        return false;
                    }
                    if (!$this->validateEmail('email', $input, $error)) {
                        return false;
                    }
                    if (!$this->validateExists('email', $input, User::class, $error)) {
                        return false;
                    }

                    return true;
                }
            ]
        );
    }

    /**
     * @param string $attribute
     * @param array $options
     *
     * @return string
     */
    private function getValueByAttribute(string $attribute, array $options = []): string
    {
        $attributeName = ucfirst($attribute);
        $options = ArrayHelper::merge(
            [
                'required' => true,
                'error' => $this->getRequiredErrorMessage($attribute)
            ],
            $options
        );

        return Console::prompt("$attributeName: ", $options);
    }
}
