<?php

namespace miks1108\usersControl\console\services\createUser;

/**
 * Class ParamsCreator
 */
class ParamsCreator extends Creator
{
    /**
     * @return Method
     */
    public function getMethod(): Method
    {
        return new ParamsMethod();
    }
}
