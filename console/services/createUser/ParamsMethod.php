<?php

namespace miks1108\usersControl\console\services\createUser;

use Exception;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Class ParamsMethod
 */
class ParamsMethod implements Method
{
    /**
     * @return string
     * @throws Exception
     */
    public function getUsername(): string
    {
        return $this->getValueByAttribute('username');
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getPassword(): string
    {
        return $this->getValueByAttribute('password');
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getEmail(): string
    {
        return $this->getValueByAttribute('email');
    }

    /**
     * @param string $attribute
     *
     * @return string
     * @throws InvalidConfigException
     * @throws Exception
     */
    private function getValueByAttribute(string $attribute): string
    {
        $value = ArrayHelper::getValue(Yii::$app->params, "defaultUser.$attribute");
        if ($value === null) {
            $message = strtr('{attribute} not found in params', [
                '{attribute}' => ucfirst($attribute)
            ]);
            throw new InvalidConfigException($message);
        }

        return $value;
    }
}
