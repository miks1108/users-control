<?php

namespace miks1108\usersControl\console\services\createUser;

use miks1108\usersControl\UsersControlModule;
use yii\db\ActiveRecord;
use yii\helpers\Console;
use yii\validators\EmailValidator;

/**
 * Trait ManualMethodValidatorsTrait
 */
trait ManualMethodValidatorsTrait
{
    /**
     * @param string $value
     *
     * @return void
     */
    public function trim(string &$value): void
    {
        $value = trim($value);
    }

    /**
     * @param string $attribute
     * @param string $value
     * @param string|null $error
     *
     * @return bool
     */
    public function validateRequired(string $attribute, string $value, string|null &$error): bool
    {
        if (empty($value)) {
            $error = $this->getRequiredErrorMessage($attribute);

            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param string $value
     * @param string|null $error
     * @param int|null $length
     *
     * @return bool
     */
    public function validateMinLength(string $attribute, string $value, string|null &$error, int $length = null): bool
    {
        if ($length === null) {
            $length = UsersControlModule::getDefaultStringMinLength();
        }
        if (mb_strlen($value) < $length) {
            $error = $this->ansiError(
                "{attribute} must be greater than {length} characters.",
                [
                    '{attribute}' => ucfirst($attribute),
                    '{length}' => $length
                ]
            );

            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param string $value
     * @param string|null $error
     * @param int|null $length
     *
     * @return bool
     */
    public function validateMaxLength(string $attribute, string $value, string|null &$error, int $length = null): bool
    {
        if ($length === null) {
            $length = UsersControlModule::getDefaultStringMaxLength();
        }
        if (mb_strlen($value) > $length) {
            $error = $this->ansiError(
                "{attribute} must be less than {length} characters.",
                [
                    '{attribute}' => ucfirst($attribute),
                    '{length}' => $length
                ]
            );

            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param string $value
     * @param string|ActiveRecord $targetClass
     * @param string|null $error
     *
     * @return bool
     */
    public function validateExists(
        string $attribute,
        string $value,
        string|ActiveRecord $targetClass,
        string|null &$error
    ): bool {
        $result = $targetClass::find()->where([$attribute => $value])->exists();
        if ($result) {
            $error = $this->ansiError(
                "This {attribute} has already been taken.",
                ['{attribute}' => ucfirst($attribute)]
            );

            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param string $value
     * @param string|null $error
     *
     * @return bool
     */
    public function validateEmail(string $attribute, string $value, string|null &$error): bool
    {
        if (!(new EmailValidator())->validate($value)) {
            $error = $this->ansiError('{attribute} is invalid.', ['{attribute}' => ucfirst($attribute)]);

            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     *
     * @return string
     */
    private function getRequiredErrorMessage(string $attribute): string
    {
        return strtr(
            $this->ansiError('{attribute} cannot be blank.'),
            ['{attribute}' => ucfirst($attribute)]
        );
    }

    /**
     * @param string $string
     * @param array $params
     *
     * @return string
     */
    private function ansiError(string $string, array $params = []): string
    {
        if (!empty($params)) {
            $string = strtr($string, $params);
        }

        return Console::ansiFormat($string, [Console::FG_RED]);
    }
}
