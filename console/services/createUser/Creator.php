<?php

namespace miks1108\usersControl\console\services\createUser;

use common\models\User;
use miks1108\usersControl\console\services\createUser\models\CreateUserFromConsoleForm;
use miks1108\usersControl\UsersControlModule;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\helpers\Console;

/**
 * Class Creator
 */
abstract class Creator
{
    /**
     * @return Method
     */
    abstract public function getMethod(): Method;

    /**
     * @param bool $activateUser
     *
     * @return bool
     * @throws Exception|InvalidConfigException
     */
    public function create(bool $activateUser): bool
    {
        $method = $this->getMethod();

        return $this->createUser(
            $this->getUserAttributes($method),
            $activateUser
        );
    }

    /**
     * @param array $attributes
     * @param bool $activateUser
     *
     * @return bool
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function createUser(array $attributes, bool $activateUser): bool
    {
        $form = new CreateUserFromConsoleForm();
        $form->setAttributes($attributes);
        if ($activateUser) {
            $form->status = User::STATUS_ACTIVE;
        }

        try {
            return $form->create();
        } catch (Exception $exception) {
            throw new Exception(
                Console::ansiFormat("User was not created.\nError: ", [Console::FG_RED]) .
                $exception->getMessage()
            );
        }
    }

    /**
     * @param Method $method
     *
     * @return array
     */
    private function getUserAttributes(Method $method): array
    {
        $attributes = [];
        foreach (UsersControlModule::getUserAttributes() as $attribute) {
            $attributes[$attribute] = $this->getValueByAttribute($method, $attribute);
        }

        return $attributes;
    }

    /**
     * @param Method $method
     * @param string $attribute
     *
     * @return string
     */
    private function getValueByAttribute(Method $method, string $attribute): string
    {
        return match ($attribute) {
            'username' => $method->getUsername(),
            'email' => $method->getEmail(),
            'password' => $method->getPassword()
        };
    }
}
