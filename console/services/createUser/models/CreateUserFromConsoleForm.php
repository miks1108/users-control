<?php

namespace miks1108\usersControl\console\services\createUser\models;

use Exception;
use miks1108\usersControl\UsersControlModule;
use Yii;
use yii\base\{
    InvalidConfigException,
    Model
};
use common\models\User;
use yii\console\widgets\Table;
use yii\helpers\Console;

/**
 * Create user from console form
 */
class CreateUserFromConsoleForm extends Model
{
    /**
     * @var string|null
     */
    public string|null $username = null;

    /**
     * @var string|null
     */
    public string|null $email = null;

    /**
     * @var string|null
     */
    public string|null $password = null;

    /**
     * @var int
     */
    public int $status = User::STATUS_INACTIVE;

    /**
     * @var array
     */
    private array $userAttributes;

    /**
     * Initialization
     */
    public function init(): void
    {
        parent::init();
        $this->userAttributes = UsersControlModule::getUserAttributes();
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            [
                'username',
                'unique',
                'targetClass' => User::class,
                'message' => 'This username has already been taken.'
            ],
            [
                'username',
                'string',
                'min' => UsersControlModule::getUsernameMinLength(),
                'max' => UsersControlModule::getDefaultStringMaxLength()
            ],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => UsersControlModule::getDefaultStringMaxLength()],
            [
                'email',
                'unique',
                'targetClass' => User::class,
                'message' => 'This email address has already been taken.'
            ],

            ['password', 'required'],
            [
                'password',
                'string',
                'min' => UsersControlModule::getPasswordMinLength()
            ],
        ];
    }

    /**
     * @return bool whether the creating new account was successful and email was sent
     * @throws InvalidConfigException
     * @throws Exception
     */
    public function create(): bool
    {
        if (!$this->validate($this->userAttributes)) {
            throw new InvalidConfigException(
                Console::ansiFormat("Validation errors:\n", [Console::FG_RED]) .
                $this->getFormattedValidationErrors()
            );
        }
        $attributes = array_intersect_key(
            [
                'username' => $this->username,
                'email' => $this->email
            ],
            array_flip($this->userAttributes)
        );

        $user = new User($attributes);
        $user->setAttributes(UsersControlModule::getDefaultAttributes(), false);
        $user->status = $this->status;
        if (in_array('password', $this->userAttributes)) {
            $user->setPassword($this->password);
        }
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();

        return $user->save();
    }

    /**
     * @return string
     * @throws Exception
     */
    private function getFormattedValidationErrors(): string
    {
        $rows = [];
        foreach ($this->getErrors() as $attribute => $errors) {
            if (count($errors) === 1) {
                $errors = reset($errors);
            }
            $rows[] = [
                $attribute,
                $errors
            ];
        }

        return Table::widget([
            'headers' => ['Attribute', 'Errors'],
            'rows' => $rows
        ]);
    }
}
