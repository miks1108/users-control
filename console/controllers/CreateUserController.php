<?php

namespace miks1108\usersControl\console\controllers;

use Exception;
use miks1108\usersControl\console\services\createUser\{
    Creator,
    ManualCreator,
    ParamsCreator
};
use yii\console\{
    Controller,
    ExitCode
};
use yii\base\InvalidConfigException;
use yii\db\Exception as DbException;
use yii\helpers\Console;

/**
 * Class TestController
 */
class CreateUserController extends Controller
{
    /**
     * @return int|bool
     */
    public function actionIndex(): int|bool
    {
        try {
            $isUserCreated = $this->createUser($this->getCreateMethod());
            if ($isUserCreated) {
                $this->stdout('User was created successfully', Console::FG_GREEN);
                $this->stdout("\n");

                return ExitCode::OK;
            }
            $this->stderr('User was not created', Console::FG_RED);
            $this->stderr("\n");
        } catch (Exception $exception) {
            $this->stderr($exception->getMessage());
            $this->stderr("\n");
        }

        return ExitCode::UNSPECIFIED_ERROR;
    }

    /**
     * @param Creator $creator
     *
     * @return bool
     * @throws DbException
     * @throws InvalidConfigException
     */
    private function createUser(Creator $creator): bool
    {
        $confirmMessage = Console::ansiFormat('Activate new user immediately?', [Console::FG_CYAN]);
        $activateUser = Console::confirm($confirmMessage);
        $this->stdout("========================");
        $this->stdout("\n");

        return $creator->create($activateUser);
    }

    /**
     * @return Creator
     */
    private function getCreateMethod(): Creator
    {
        if (Console::confirm(Console::ansiFormat('Create user from params?', [Console::FG_CYAN]))) {
            return new ParamsCreator();
        }

        return new ManualCreator();
    }
}
