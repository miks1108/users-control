<?php

namespace miks1108\usersControl;

use Yii;
use yii\base\Module;

/**
 * Class UsersControlModule
 *
 * @method static getUserAttributes
 * @method static getDefaultAttributes
 * @method static getUsernameMinLength
 * @method static getPasswordMinLength
 * @method static getDefaultStringMinLength
 * @method static getDefaultStringMaxLength
 */
class UsersControlModule extends Module
{
    /**
     * @var array
     */
    public array $userAttributes = [
        'username',
        'email',
        'password'
    ];

    /**
     * @var array
     */
    public array $defaultAttributes = [];

    /**
     * @var int
     */
    public int $defaultStringMinLength = 3;

    /**
     * @var int
     */
    public int $usernameMinLength = 2;

    /**
     * @var int
     */
    public int $passwordMinLength = 5;

    /**
     * @var int
     */
    public int $defaultStringMaxLength = 255;

    /**
     * Initialization
     */
    public function init(): void
    {
        parent::init();
        if (Yii::$app instanceof yii\console\Application) {
            $this->controllerNamespace = 'miks1108\usersControl\console\controllers';
        }
    }

    /**
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic(string $name, array $arguments): mixed
    {
        if (strncmp($name, 'get', 3) === 0) {
            $name = lcfirst(ltrim($name, 'get'));
            $instance = self::getInstance();
            if ($instance->hasProperty($name) || $instance->hasMethod($name)) {
                return $instance->$name;
            }
        }

        return null;
    }
}
